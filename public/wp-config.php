<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kjlkUpZHXP9e6gXdI+4ZeMHW3ono2Vy2tRqXECPGafE0Bikhc2rqFlk0y/jI/6OgChIYbblGMd5SWLCtPVTKQQ==');
define('SECURE_AUTH_KEY',  'IZ6dKgJfBm4gbIMfPEiDQ1DsmodJZ7PJOnDzlvSonw6jHNTmkUtlv6VAccUX9jLpm02shIeHuI6Aq+/2zezzXA==');
define('LOGGED_IN_KEY',    'EmjXWLvuOZEGNq27K47Ixn8mBgr7QNVqIJE3FtcjcOaRZPOf2E6bXlpsMj9feKhwXXlwbm5/8HnROXfJ/DcvAQ==');
define('NONCE_KEY',        'sN80nuikxxKGtj3ZUrEE+ic1FIcBg6wQY1pu0G1Mqjx4GpQHjZxDkN+WCwUZVpuzyOpOBmK4IfxJdheqCnmbwQ==');
define('AUTH_SALT',        '5F15KEVvbh0q60m2joECIdzM7ECyE4rFoAoB7IZnLpjc/EWS7IMotE6t4xDzaWsd59IxSBFbM106Lg4YYjkKiQ==');
define('SECURE_AUTH_SALT', '6WppJ+x97B4oZtyRKxkF7jQPKCX9rrFPVrC8qIXPyfTZHdCpXV3LY20kToC46/iiRSE5BXDu2/mxGc5kmO5IoA==');
define('LOGGED_IN_SALT',   'm2e4M6GGLh+F+wE61TNfltRQlUA7RedUV7A4l22xp2BRpRHsLSpoInrafrfVJJ2upo5I+IvaBraLYlaVErI5hQ==');
define('NONCE_SALT',       'q5+hjZjZuGomW6dqsDYprE/s++IP0lUbPIygnd9Qnu9O0spTdgpXizOTowN/cEDfQLTpZtSm579V6RC33YvQkA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
