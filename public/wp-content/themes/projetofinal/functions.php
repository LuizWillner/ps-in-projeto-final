<?php

    // Importando minha stylesheet
    function projetofinal_css() {
        wp_register_style('projetofinal_style', get_template_directory_uri() . 'style.css', [], '1.0.0', false);
        wp_enqueue_style('projetofinal_style');
    }

    add_action('wp_enqueue_scripts', 'projetofinal_css');



    // Importando JS  da animação do carrinho
    function cart_side_bar() {
        wp_enqueue_script('cart', get_template_directory_uri() . '/js/cart.js');
    }
    
    add_action('wp_enqueue_scripts', 'cart_side_bar');



    // CSS padrão do WC anulado
    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );



// ============================ PÁGINA SHOP ================================

    // Removendo menu de navegação desnecessário do topo da página SHOP
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

    // Removendo menu de navegação desnecessário da side bar do SHOP
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10, 0);


    
    // HOOK: criando barra de categorias na página do SHOP
    add_action( 'woocommerce_archive_description', 'make_categories_bar' );
    function make_categories_bar() {
        // Pegando url da página
        $protocolo = ( isset( $_SERVER['HTTPS'] ) && ( $_SERVER['HTTPS']=="on" ) ? "https" : "http" );
        $url = '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];


        echo '<div class="shop-categories-selection">';
            $taxonomy     = 'product_cat';
            $orderby      = 'name';  
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no  
            $title        = '';  
            $empty        = 0;

            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
            );

            $all_categories = get_categories( $args );

            foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $category_id = $cat->term_id;
                    $image = wp_get_attachment_image( $thumbnail_id );
                    
                    // Determinando qual categoria vai ficar com a borda roxa da seleção
                    if ( $protocolo.$url === get_term_link( $cat->slug, 'product_cat' ) ){
                        echo '<a class="smooth-button img-button highlight-category" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="cat-name purple-degrade">'.$cat->name.'</p></a>';
                    }

                    else {
                        echo '<a class="smooth-button img-button" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="cat-name purple-degrade">'.$cat->name.'</p></a>';
                    }
                }       
            }
        echo '</div>';

        
        // Escrevendo na tela qual categoria está selecionada
        echo '<div class="category-name">';
        echo '<h1 class="choose-a-category">PRATOS</h1>';
        switch ($protocolo.$url) {
            case 'http://comesebebes.local/product-category/japanese-food/':
                echo '<p>COMIDA JAPONESA</p>';
                break;
            case 'http://comesebebes.local/product-category/pasta/':
                echo '<p>MASSAS</p>';
                break;
            case 'http://comesebebes.local/product-category/nordestina/':
                echo '<p>COMIDA NORDESTINA</p>';
                break;
            case 'http://comesebebes.local/product-category/vegan-food/':
                echo '<p>COMIDA VEGANA</p>';
                break;
            case 'http://comesebebes.local/shop/':
                echo '<p>TODOS</p>';
                break;
        };
        echo '<div><h3>Ordenar por:</h3><h3>Filtro de preço:</h3></div>';
        echo '</div>';


        /**
         * The template for displaying product price filter widget.
         *
         * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-price-filter.php
         *
         * HOWEVER, on occasion WooCommerce will need to update template files and you
         * (the theme developer) will need to copy the new files to your theme to
         * maintain compatibility. We try to do this as little as possible, but it does
         * happen. When this occurs the version of the template file will be bumped and
         * the readme will list any important changes.
         *
         * @see     https://docs.woocommerce.com/document/template-structure/
         * @package WooCommerce\Templates
         * @version 3.7.1
         */

        defined( 'ABSPATH' ) || exit;

        do_action( 'woocommerce_widget_price_filter_start', $args );

        echo '<div class="orderby-and-filtering">';

            echo woocommerce_catalog_ordering();

            // Filtragem de preço
            echo '<form method="get" action="' . esc_url( $form_action ) . '">';
            echo    '<div class="price_slider_wrapper">';
            echo        '<div class="price_slider" style="display:none;"></div>';
            echo        '<div class="price_slider_amount" data-step="' . esc_attr( $step ) . '">';
            echo            '<label for="min_price">De:</label>';
            echo            '<input type="text" id="min_price" name="min_price" value="' . esc_attr( $current_min_price ) . '" data-min="' . esc_attr( $min_price ) . '" />';
            echo            '<label for="max_price">Até:</label>';
            echo            '<input type="text" id="max_price" name="max_price" value="' . esc_attr( $current_max_price ) . '" data-max="' . esc_attr( $max_price ) . '" />';
                            /* translators: Filter: verb "to filter" */
            echo            '<button type="submit" class="filter-button smooth-button">' . esc_html__( 'Filtrar', 'woocommerce' ) . '</button>';
            echo            '<div class="price_label" style="display:none;">';
            echo                esc_html__( 'Price:', 'woocommerce' ) . '<span class="from"></span> &mdash; <span class="to"></span>';
            echo            '</div>';
            echo            wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true );
            echo            '<div class="clear"></div>';
            echo        '</div>';
            echo    '</div>';
            echo '</form>';

            do_action( 'woocommerce_widget_price_filter_end', $args );

        echo "</div>";
    }
        


    // Criando div container que vai englobar toda a lista + paginação
    add_action('woocommerce_before_shop_loop', 'open_div_for_show_products');
    function open_div_for_show_products() {
        echo '<div class="show-products">';
    }

    // Fechando a div criada
    add_action('woocommerce_after_shop_loop', 'close_div_for_show_products');
    function close_div_for_show_products() {
        echo '</div>';
    }



    // Removendo nome
    remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');

    // Removendo preço
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');

    // Removendo botão add to cart
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

    // Removendo essa filtragem desnecessária, pois já adicionei outra ali em cima
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );



    // Criando a descição do produto (nome + preço + add cart) para os produtos da shop
    add_action('woocommerce_shop_loop_item_title', 'create_dish_info');
    function create_dish_info() {
        echo        '<div class="purple-degrade dish-info">';
        echo            '<p>'.woocommerce_template_loop_product_title().'</p>';
        echo            '<div>';
        echo                '<p>'.woocommerce_template_loop_price().'</p>';
        echo                '<img class="smooth-button img-button cart-button" src="' . get_stylesheet_directory_uri() . '/images/add_cart.png" width="30" height="30">';          
        echo            '</div>';
        echo        '</div>';
    }



// =========================== PÁGINA SINGLE PRODUCT =============================

    // Zoom na foto do produto
    add_theme_support( 'wc-product-gallery-zoom' );

    remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',30,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40,0);

    add_action( 'woocommerce_before_single_product_summary', 'open_div_single_product' );
    function open_div_single_product() {
        echo '<div class="product-img-and-info">';
    }
    
    add_action( 'woocommerce_after_single_product_summary', 'close_div_and_create_related_product_title', 10, 1 );
    function close_div_and_create_related_product_title($product) {
        $args = array(
            'orderby' => 'name',
        );
        $product_cats = wp_get_post_terms( $product->get_id(), 'product_cat', $args );
        $cat_properties = get_object_vars($product_cats[0]);

        echo '</div>';   

        switch ($cat_properties['name']) {
            case 'Vegana':
                echo '<h3 class="related-products-cat">MAIS COMIDA VEGANA</h3>';
                break;
            
            case 'Japonesa':
                echo '<h3 class="related-products-cat">MAIS COMIDA JAPONESA</h3>';
                break;

            case 'Massas':
                echo '<h3 class="related-products-cat">MAIS MASSAS</h3>';
                break;
            
            case 'Nordestina':
                echo '<h3 class="related-products-cat">MAIS COMIDA NORDESTINA</h3>';
                break;
        }
    }


    add_action( 'woocommerce_single_product_summary', 'create_product_summary' );
    function create_product_summary() {
        echo woocommerce_template_single_title();
        echo woocommerce_template_single_excerpt();
        echo woocommerce_template_single_add_to_cart();
    }



?>