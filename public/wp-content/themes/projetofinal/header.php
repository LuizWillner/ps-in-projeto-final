<!DOCTYPE html>
<html lang="pt-br">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bellota+Text:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <title> <?php bloginfo('name');?> <?php wp_title('|');?> </title>
    <?php wp_head(); ?>
</head>


<body>
    <header class="main-header">

        <section class="logo-and-search">
            <a href="/landing-page/" class="smooth-button img-button"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/comes-e-bebes-logo.png"></a>
            <!-- <div class="search-bar">
                <label for="barraDePesquisa"><img src="<?php echo get_stylesheet_directory_uri()?>/images/lupa.png" id="lupa"></label>
                <input type="search" id="barraDePesquisa">
            </div> -->
            <form action="<?php echo bloginfo("url");?>/product/" method="get" class="search-bar">
                <input type="image" src="<?php echo get_stylesheet_directory_uri()?>/images/lupa.png" id="lupinha">
                <input type="text" name="s" cat="s" placeholder="Pesquisar..." value="<?php echo the_search_query( )?>">
            </form>
        </section>

        <section class="header-option-container">
            <a href="/shop/" class="smooth-button yellow-button">Faça um pedido</a>
            <a class="smooth-button img-button" onclick="openNav()"><img src="<?php echo get_stylesheet_directory_uri()?>/images/cart.png"></a>
            <a href="/my-account/" class="smooth-button img-button"><img src="<?php echo get_stylesheet_directory_uri()?>/images/profile.png"></a>
        </section>

        <div id="cartSidenav" class="sidenav">
            <a href="javascript:void( 0 )" class="closebtn" onclick="closeNav()">&times;</a>
            <h1>CARRINHO</h1>

            <div class="cart-content">
                <?php
                $my_cart = WC()->cart->get_cart();

                foreach ( $my_cart as $cart_item ) {
                    $item_name = mb_strtoupper( $cart_item['data']->get_title(), mb_internal_encoding() );
                    $quantity = $cart_item['quantity'];
                    $price = $cart_item['data']->get_price_html();
                    $image = $cart_item['data']->get_image();
                    // $id = $cart_item['data']->get_id();

                    echo '<div class="cart-item-card">';

                    echo    $image;

                    echo    '<div>';
                    echo        '<h3>' . $item_name . '</h3>';

                    echo        '<div>';
                    echo            '<p>x' . $quantity . '</p>';
                    echo            '<p>' . $price . '</p>';
                    echo        '</div>';            
                    echo    '</div>';

                    echo '</div>';
                }
                ?>
            </div>

            <?php
                $total = WC()->cart->get_cart_total();
                echo '<p>Total do carrinho: ' . $total . '</p>';
            ?>

            <div class="buy-button">
                <a href="/checkout/" class="smooth-button purple-button">Comprar</a>
            </div>
        </div>

        <!-- <div id="main">
            <h2>Sidenav Push Example</h2>
            <p>Click on the element below to open the side navigation menu, and push this content to the right. Notice that we add a black see-through background-color to body when the sidenav is opened.</p>
            <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>
        </div> -->


    </header>