<?php /* Template Name: Landing page*/ ?>
<div id="screenOpacity"></div>
<?php get_header(); ?>

    <main>
        <section class="subheader-landing-page">
            <div>
                <h1>Comes&Bebes</h1>
                <p>O restaurante pra todas as fomes</p>
            </div>
        </section>

        <section class="section-conheca-nossa-loja">
            <h1>CONHEÇA NOSSA LOJA</h1>

            <section class="categories-subsection">
                <h2>Tipos de pratos principais</h2>

                <div class="categories-selection">
                    <?php
                        $taxonomy     = 'product_cat';
                        $orderby      = 'name';  
                        $show_count   = 0;      // 1 for yes, 0 for no
                        $pad_counts   = 0;      // 1 for yes, 0 for no
                        $hierarchical = 1;      // 1 for yes, 0 for no  
                        $title        = '';  
                        $empty        = 0;

                        $args = array(
                            'taxonomy'     => $taxonomy,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty
                        );

                        $all_categories = get_categories( $args );

                        foreach ($all_categories as $cat) {
                            if($cat->category_parent == 0) {
                                $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                                $category_id = $cat->term_id;
                                $image = wp_get_attachment_image( $thumbnail_id );
                                echo '<a class="smooth-button img-button" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="cat-name purple-degrade">'.$cat->name.'</p></a>';
                                
                            }       
                        }
                    ?>
                </div>

            </section>

            <section class="daydishes-subsection">
                <h2>Pratos do dia de hoje</h2>
                <?php
                    setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese' );
                    date_default_timezone_set( 'America/Sao_Paulo' );
                    $day_num=strftime( ucwords( "%w",time() ) ); 

                    global $day;
                    switch ($day_num) {
                        case "1":
                            $day = 'Segunda';
                            echo '<h3>SEGUNDA</h3>';;
                            break;
                        case "2":
                            $day = 'Terça';
                            echo '<h3>TERÇA</h3>';;
                            break;
                        case "3":
                            $day = 'Quarta';
                            echo '<h3>QUARTA</h3>';;
                            break;
                        case "4":
                            $day = 'Quinta';
                            echo '<h3>QUINTA</h3>';;
                            break;
                        case "5":
                            $day = 'Sexta';
                            echo '<h3>SEXTA</h3>';;
                            break;
                        case "6":
                            $day = 'Sábado';
                            echo '<h3>SÁBADO</h3>';;
                            break;
                        case "0":
                            $day = 'Domingo';
                            echo '<h3>DOMINGO</h3>';;
                            break;
                    }
                ?>

                <div class="all-daydishes">
                    <?php
                        $args = array(
                            'status' => 'publish',
                            'limit'  => -1 
                        );

                        $all_products = wc_get_products( $args );

                        foreach ( $all_products as $product ) {
                            $product_id = $product->get_id();
                            $product_tags =  wc_get_product_tag_list( $product_id, $sep = ' ', $before = '', $after = '' );

                            if( strpos( $product_tags, $day ) !== false ) {  // verificando se a tag do produto é referente ao dia de hoje
                                $product_image =  woocommerce_get_product_thumbnail();
                                $product_link = get_permalink( $product_id );
                                $product_name = $product->get_name();
                                $product_price = $product->get_price_html();
                                $product_slug = $product->get_slug();

                                echo '<div class="smooth-button img-button dish-card">';
                                echo    '<a href="' . $product_link . '">' . $product_image . '</a>';
                                
                                echo    '<div class="purple-degrade dish-info">';
                                echo        '<p>' . $product_name . '</p>';
                                echo        '<div>';
                                echo            '<p>' . $product_price . '</p>';
                                echo            '<a class="smooth-button img-button cart-button" href="/product/' . $product_slug . '"><img src="' . get_stylesheet_directory_uri() . '/images/add_cart.png" width="30" height="30"></a>';
                                echo        '</div>';
                                echo    '</div>';        
                                echo '</div>';
                            }
                        }

                    ?>

                </div>
            </section>

            <a href="/shop/" class="smooth-button yellow-button" id="outrasOpcoes">Veja outras opções</a>

        </section>

        <section class="subfooter-landing-page">
            <h1>VISITE NOSSA LOJA FÍSICA</h1>

            <div>
                <div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1829563564884!2d-43.11302338508294!3d-22.90662168501186!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983e3cf58a855%3A0x9b5dc41cea7ae8c1!2sShopping%20Icara%C3%AD!5e0!3m2!1spt-BR!2sbr!4v1634059495593!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <div>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/restaurant.png" alt="">
                        <p>Rua lorem ipsum, 123, LI, Brasil</p>
                    </div>

                    <div>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/telephone.png" alt="">
                        <p>(XX) XXXX-XXXX</p>
                    </div>
                </div>
                
                <ul class="slider">          
                    <li>
                        <input type="radio" id="slide1" name="slidef" checked>
                        <label for="slide1"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/pessoas-comendo-1.jpg" alt="" width="600" height="400">
                    </li>
                        
                    <li>
                        <input type="radio" id="slide2" name="slidef" checked>
                        <label for="slide2"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/pessoas-comendo-2.jpg" alt="" width="600" height="400">
                    </li>
                    
                    <li>
                        <input type="radio" id="slide3" name="slidef" checked>
                        <label for="slide3"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/pessoas-comendo-3.jpg" alt="" width="600" height="400">
                    </li>
                </ul>

            </div>

        </section>
    </main>
    <?php get_footer(); ?>