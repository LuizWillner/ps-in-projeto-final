<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="myaccount-nav">
	<a href="/my-account/editar-conta/" class="smooth-button yellow-button"><h1>PAINEL</h1></a>
	<a href="/my-account/pedidos/" class="smooth-button yellow-button"><h1>PEDIDOS</h1></a>
	<a href="/my-account/editar-endereco/" class="smooth-button yellow-button"><h1>ENDEREÇO</h1></a>
	<a href="/my-account/logout/" class="smooth-button yellow-button"><h1>SAIR</h1></a>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
